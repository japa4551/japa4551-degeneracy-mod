using HarmonyLib;
using Verse;
using System;
using System.Reflection;

namespace Degeneracy_RJW_Genes
{
	[StaticConstructorOnStartup]
	internal static class InitHarmonyPatches
	{
		static InitHarmonyPatches()
		{
			var har = new Harmony("Degeneracy.RJW_Genes");
			har.PatchAll(Assembly.GetExecutingAssembly());
		}
	}
}
