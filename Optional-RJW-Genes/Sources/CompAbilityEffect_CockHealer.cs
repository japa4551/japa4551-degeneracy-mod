﻿using HarmonyLib;
using Verse;
using RimWorld;
using rjw;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace Degeneracy_RJW_Genes
{
	public class CompAbilityEffect_CockHeal : CompAbilityEffect
	{
		private new CompProperties_AbilityCockHeal Props
		{
			get
			{
				return (CompProperties_AbilityCockHeal)this.props;
			}
		}
		public void TryHealing(Pawn pawn, bool was_raped = false) {
			bool any_wound_tended = RJW_Genes.AbilityUtility.Heal(pawn, this.Props.tendQualityRange);
			if (any_wound_tended)
			{
				MoteMaker.ThrowText(pawn.DrawPos, pawn.Map, "Sex tended wounds", 3.65f);

				if (was_raped) {
					pawn.needs.mood.thoughts.memories.TryGainMemory(
						ThoughtDefOf.japa4551_RJW_Genes_CockHealer_Rape, pawn, null
					);
				}

				else {
					pawn.needs.mood.thoughts.memories.TryGainMemory(
						ThoughtDefOf.japa4551_RJW_Genes_CockHealer_Casual, pawn, null
					);
				}
			}
		}
		public override void Apply(LocalTargetInfo target, LocalTargetInfo dest)
		{
			base.Apply(target, dest);
			Pawn pawn = target.Pawn;
			if (pawn == null)
			{
				return;
			}

			this.TryHealing(pawn, true);
		}

		// public void AfterSex(Pawn pawn, Pawn target)
		// {
		// 	Log.Message("Hello");
			// List<Hediff> hediffs = target.health.hediffSet.hediffs;
			// List<Hediff> hediffs = target.health.hediffSet.hediffs;
			// for (int i = 0; i < hediffs.Count; i++)
			// {
			// 	Log.Message(hediffs[i].Label);

			// 	if ((hediffs[i] is Hediff_Injury || hediffs[i] is Hediff_MissingPart) && hediffs[i].TendableNow(false))
			// 	{
            //         //target.needs.mood.thoughts.memories.TryGainMemory(ThoughtDefOf.Pussy_Healed, pawn, null);
            //         break;
			// 	}
			// }
			

			// if(hediffs.Any(e => e is Hediff_Injury && e.def.tendable))
			// {
			// 	this.TryHealing(target);
			// }
		// }

		// Defines if the pawn can be selected
		public override bool Valid(LocalTargetInfo target, bool throwMessages = false)
		{
			Pawn caster_pawn = this.parent.pawn;
			Pawn target_pawn = target.Pawn;

			if (target_pawn != null)
			{
				// Checks if target pawn is wounded, if its not ignore it
				if (!target_pawn.health.hediffSet.hediffs.Any(e => (e is Hediff_Injury || e is Hediff_MissingPart) && e.TendableNow())
				){
					if (throwMessages) {
						Messages.Message(target_pawn.Name + " cannot be tended.", target_pawn, MessageTypeDefOf.RejectInput, false);
					}

					return false;
				}

				// Checks if the pawn is capable of having sex
				else if (!CasualSex_Helper.CanHaveSex(target_pawn))
				{
					if (throwMessages) {
						Messages.Message(target_pawn.Name + " is unable to have sex.", target_pawn, MessageTypeDefOf.RejectInput, false);
					}

					return false;
				}

				// If the target is a Animal: Checks if Bestiality is enabled
				else if (target_pawn.IsAnimal() && !RJWSettings.bestiality_enabled)
				{
					if (throwMessages) {
						Messages.Message("bestiality is disabled.", target_pawn, MessageTypeDefOf.RejectInput, false);
					}

					return false;
				}

				// Checks if target pawn has the minimum age, should work with custom races
				else if(!RJWSettings.AllowYouthSex)
				{
					bool casterAgeOk = caster_pawn.ageTracker.Growth == 1;
					bool targetAgeOk = target_pawn.ageTracker.Growth == 1;
					if(!casterAgeOk || !targetAgeOk)
					{
						if (throwMessages) {
							Messages.Message(target_pawn.Name + " is a child!", target_pawn, MessageTypeDefOf.RejectInput, false);
						}

						return false;
					}
				}
			}

			return base.Valid(target, throwMessages);
		}

		// Defines when the ability is clickable
		public override bool GizmoDisabled(out string reason)
		{
			var pawn = this.parent.pawn;
			reason = null;
			
			// If it does not have pee pee
			if (!Genital_Helper.has_male_bits(pawn))
			{
				reason = pawn.Name + " has no penis.";
				return true;
			}

			else if (!RJWSettings.rape_enabled)
			{
				reason = "Rape is disabled";
				return true;
			}

			return false;
		}
	}

	// WIP Healing after casual sex
	// [HarmonyPatch(typeof(AfterSexUtility), "think_about_sex", new Type[] { 
	// 	typeof(Pawn), typeof(Pawn)
	// })]

	// [HarmonyPatch(typeof(AfterSexUtility), "think_about_sex", new Type[] { 
	// 	typeof(Pawn), typeof(Pawn), typeof(bool), typeof(SexProps), typeof(bool) 
	// })]

	// [StaticConstructorOnStartup]
	// static class CockHealer_AfterSexUtility_think_about_sex
	// {
	// 	[HarmonyPostfix]
	// 	public static void think_about_sex_Patch(
	// 		Pawn pawn, 
	// 		Pawn partner, 
	// 		CompAbilityEffect_CockHeal ___instance
	// 	)
	// 	{
	// 		if (partner != null)
	// 		{
	// 			RJW_Genes.AbilityUtility.Heal(partner, ___instance.Props.tendQualityRange);
	// 		}
	// 	}
	// }
}
