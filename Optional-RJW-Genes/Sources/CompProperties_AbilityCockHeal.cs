﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;

namespace Degeneracy_RJW_Genes
{
	public class CompProperties_AbilityCockHeal : CompProperties_AbilityEffect
	{
		public CompProperties_AbilityCockHeal()
		{
			this.compClass = typeof(CompAbilityEffect_CockHeal);
		}

		public FloatRange tendQualityRange;
	}
}
